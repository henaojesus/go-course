package main

import (
	"fmt"
	"net/http"
	"time"
)

func checkServer(servidor string) {
	_, err := http.Get(servidor)
	if err != nil {
		fmt.Println("El servidor no esta disponible")
	} else {
		fmt.Println("El servidor funciona correctamente")
	}
}

func main() {
	start := time.Now()
	servers := []string{
		"http://platzi.com",
		"http://google.com",
		"http://facebook.com",
		"http://instagram.com",
	}
	for _, servidor := range servers {
		checkServer(servidor)
	}
	elapsedTime := time.Since(start)
	fmt.Printf("Ejecución: %s\n", elapsedTime)
}

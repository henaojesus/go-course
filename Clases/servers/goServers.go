package main

import (
	"fmt"
	"net/http"
	"time"
)

func checkServer(server string, channel chan string) {
	_, err := http.Get(server)
	if err != nil {
		/* El canal va a tomar lo que esta a su derecha y lo va a transmitir */
		channel <- server + " no se encuentra disponible"
	} else {
		channel <- server + " esta funcionando"
	}
}

func main() {
	start := time.Now()
	channel := make(chan string)
	servers := []string{
		"http://platzi.com",
		"http://google.com",
		"http://facebook.com",
		"http://instagram.com",
	}

	i := 0

	for {
		if i > 2 {
			break
		}
		for _, server := range servers {
			go checkServer(server, channel)
		}
		time.Sleep(1 * time.Second)
		fmt.Println(<-channel)
		i++
	}

	elapsedTime := time.Since(start)
	fmt.Printf("Ejecución: %s\n", elapsedTime)
}

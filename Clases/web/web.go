package main

import (
	"fmt"
	"io"
	"net/http"
)

type webWritter struct{}

//Write recibe un slice de tipo byte y devuelve un entero y un error
func (webWritter) Write(p []byte) (int, error) {
	fmt.Println(string(p))
	return len(p), nil
}

func main() {
	response, err := http.Get("https://google.com")
	if err != nil {
		fmt.Println(err)
	}
	w := webWritter{}
	io.Copy(w, response.Body)
}

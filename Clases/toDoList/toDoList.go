package main

import "fmt"

type taskList struct {
	tasks []*task
}

func (tl *taskList) addTask(t *task) {
	tl.tasks = append(tl.tasks, t)
}

func (tl *taskList) removeTask(index int) {
	tl.tasks = append(tl.tasks[:index], tl.tasks[index+1:]...)
}

func (tl *taskList) printList() {
	for _, task := range tl.tasks {
		fmt.Println("Name: ", task.name)
		fmt.Println("Description: ", task.description)
	}
}

func (tl *taskList) printCompletedTasks() {
	for _, task := range tl.tasks {
		if task.completed {
			fmt.Println("Name: ", task.name)
			fmt.Println("Description: ", task.description)
		}
	}
}

type task struct {
	name        string
	description string
	completed   bool
}

func (t *task) checkCompleted() {
	t.completed = true
}

func (t *task) updateDescription(description string) {
	t.description = description
}

func (t *task) updateName(name string) {
	t.name = name
}

func main() {
	//&task busca la referencia en la memoria
	t1 := &task{
		name:        "Completar curso de Go",
		description: "Completar curso de Go de Platzi en esta semana",
	}
	t2 := &task{
		name:        "Completar curso de Python",
		description: "Completar curso de Python de Platzi en esta semana",
	}
	t3 := &task{
		name:        "Completar curso de Javascript",
		description: "Completar curso de Javascript de Platzi en esta semana",
	}
	/* fmt.Printf("%+v\n", t)
	t.checkCompleted()
	t.updateName("Finalizar curso Go")
	t.updateDescription("Completar curso cuanto antes")
	fmt.Printf("%+v\n", t) */
	lista := &taskList{
		tasks: []*task{
			t1, t2,
		},
	}

	fmt.Println(lista.tasks[0])
	lista.addTask(t3)
	lista.tasks[0].checkCompleted()
	/* fmt.Println(len(lista.tasks))
	lista.removeTask(1)
	fmt.Println(len(lista.tasks)) */

	/* for i := 0; i < len(lista.tasks); i++ {
		fmt.Println("Index: ", i, "Task: ", lista.tasks[i].name)
	}*/

	/* for i := 0; i < 10; i++ {
		if i == 5 {
			break
		}
		fmt.Println(i)
	}
	for i := 0; i < 10; i++ {
		if i == 5 {
			//Continue skips iteration
			continue
		}
		fmt.Println(i)
	}*/
	lista.printList()
	fmt.Println("-----Tareas Completadas-----")
	lista.printCompletedTasks()

	maptasks := make(map[string]*taskList)
	maptasks["Jesus"] = lista

	t4 := &task{
		name:        "Completar curso de Java",
		description: "Completar curso de Java de Platzi en esta semana",
	}
	t5 := &task{
		name:        "Completar curso de html",
		description: "Completar curso de html de Platzi en esta semana",
	}
	lista2 := &taskList{
		tasks: []*task{
			t4, t5,
		},
	}
	maptasks["Ricardo"] = lista2

	fmt.Println("Tareas de Jesus: ----")
	maptasks["Jesus"].printList()
	fmt.Println("Tareas de Ricardo: ----")
	maptasks["Ricardo"].printList()
}

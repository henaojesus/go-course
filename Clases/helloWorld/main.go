package main //Nombre del Paquete

import "fmt" //Importar librerías necesarias para ejecutar el programa

func main(){
   fmt.Println("Hola Mundo desde Go")
}

func privada(){
	fmt.Println("Función en minúscula, que no será o no podrá ser exportada")
}

func Publica(){
	fmt.Println("Función en mayúscula, que podrá ser exportada")
}
package main

import "fmt"

func main() {
	x := 25
	fmt.Println(x)
	fmt.Println(&x)
	//&x obtiene la dirección de memoria de dicha variable
	y := &x
	fmt.Println(y)
	//*y obtiene el valor de una variable en una direccion de memoria
	fmt.Println(*y)
}

func changeValue(a int) {
	fmt.Println(&a)
	a = 36
}

package main

import "net/http"

type Server struct {
	port   string
	router *Router
}

//Instanciar Servidor con el puerto al que va a apuntar
func NewServer(port string) *Server {
	return &Server{
		port:   port,
		router: NewRouter(),
	}
}

func (serv *Server) Handle(method string, path string, handler http.HandlerFunc) {
	_, urlExist := serv.router.rules[path]
	if !urlExist {
		serv.router.rules[path] = make(map[string]http.HandlerFunc)
	}
	serv.router.rules[path][method] = handler
}

//Al asignar "...Middleware" se le dice a Go que podrían llegar una cantidad desconocida de Middlewares
func (serv *Server) AddMiddleware(f http.HandlerFunc, middlewares ...Middleware) http.HandlerFunc {
	for _, middle := range middlewares {
		f = middle(f)
	}
	return f
}

//Funcion del server para escuchar peticiones
func (serv *Server) Listen() error {
	http.Handle("/", serv.router)
	//ListenAndServe recibe un puerto y un handler
	err := http.ListenAndServe(serv.port, nil)
	if err != nil {
		return err
	}
	return nil
}

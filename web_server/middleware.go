package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

func CheckAuth() Middleware {
	return func(f http.HandlerFunc) http.HandlerFunc {
		return func(writter http.ResponseWriter, request *http.Request) {
			flag := true
			fmt.Println("Checking authentication")
			if flag {
				f(writter, request)
			} else {
				return
			}
		}
	}
}

func Logging() Middleware {
	return func(f http.HandlerFunc) http.HandlerFunc {
		return func(writter http.ResponseWriter, request *http.Request) {
			start := time.Now()
			//Defer permite retrasar la ejecución de una funcion hasta al final
			defer func() {
				log.Println(request.URL.Path, time.Since(start))
			}()
			f(writter, request)
		}
	}
}

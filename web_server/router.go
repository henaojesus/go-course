package main

import (
	"net/http"
)

type Router struct {
	rules map[string]map[string]http.HandlerFunc
}

func NewRouter() *Router {
	return &Router{
		rules: make(map[string]map[string]http.HandlerFunc),
	}
}

func (r *Router) FindHandler(path string, method string) (http.HandlerFunc, bool, bool) {
	_, urlExist := r.rules[path]
	handler, methodExist := r.rules[path][method]
	return handler, methodExist, urlExist
}

func (r *Router) ServeHTTP(writter http.ResponseWriter, request *http.Request) {
	handler, methodExist, urlExist := r.FindHandler(request.URL.Path, request.Method)
	if !urlExist {
		writter.WriteHeader(http.StatusNotFound)
		return
	}
	if !methodExist {
		writter.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	handler(writter, request)
}

package main

import (
	"encoding/json"
	"net/http"
)

type Middleware func(http.HandlerFunc) http.HandlerFunc

type User struct {
	Name  string `json:"name"`
	Email string `json:"email"`
	Phone string `json:"phone"`
}

//Se usa asterisco para evitar generar copias
func (user *User) ToJson() ([]byte, error) {
	//Marshal inicia el encode de un struct a un Json
	return json.Marshal(user)
}

type MetaData interface{}

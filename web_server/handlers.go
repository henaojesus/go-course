package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

/* ResponseWriter es un método usado por un handlerHTTP para construir una respuesta */
func HandleRoot(writter http.ResponseWriter, request *http.Request) {
	fmt.Fprintf(writter, "Hello World!")
}

func HandleHome(writter http.ResponseWriter, request *http.Request) {
	fmt.Fprintf(writter, "This is the API Endpoint")
}

func PostRequest(writter http.ResponseWriter, request *http.Request) {
	//Decodificador de JSON
	decoder := json.NewDecoder(request.Body)
	var metadata MetaData
	//Decode espera una interface
	err := decoder.Decode(&metadata)
	if err != nil {
		//Con %v se le indica a go que debe parsear el codigo de error para que sea legible
		fmt.Fprintf(writter, "Error: %v", err)
		return
	}
	fmt.Fprintf(writter, "Payload %v\n", metadata)
}

func UserPostRequest(writter http.ResponseWriter, request *http.Request) {
	//Decodificador de JSON
	decoder := json.NewDecoder(request.Body)
	var user User
	//Decode espera una interface
	err := decoder.Decode(&user)
	if err != nil {
		//Con %v se le indica a go que debe parsear el codigo de error para que sea legible
		fmt.Fprintf(writter, "Error: %v", err)
		return
	}
	response, err := user.ToJson()
	if err != nil {
		writter.WriteHeader(http.StatusInternalServerError)
		return
	}
	writter.Header().Set("Content-Type", "application/json")
	//Write espera un slice de bytes
	writter.Write(response)
}
